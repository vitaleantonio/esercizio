<?php

namespace App\Http\Controllers;

use App\Car;
use Illuminate\Http\Request;
use App\Person;

class PersonController extends Controller
{

    public function insert(Request $request)
    {

        $newPerson = new Person();
        $name = $request->input('nome');
        $surname = $request->input('cognome');
        $carid = $request->input('macchina');

        $newPerson->name = $name;
        $newPerson->surname = $surname;

        $newPerson->save();

        if($carid != "err"){

            $car = Car::find($carid);
            $newPerson->car()->attach($car);

        }

        return redirect('/car');

    }

    public function register(){

        $cars = Car::all();

        return view('new-owner')->with('data',$cars);

    }

}
