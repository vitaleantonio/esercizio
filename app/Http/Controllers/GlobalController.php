<?php

namespace App\Http\Controllers;

use App\Car;
use Illuminate\Http\Request;

class GlobalController extends Controller
{

    public function show(){

        $info = Car::all();

        return view('show')->with('data',$info);

    }

}
