<?php

namespace App\Http\Controllers;

use App\Person;
use Illuminate\Http\Request;
use App\Car;

class CarController extends Controller
{

    public function insert(Request $request)
    {

        $newCar = new Car();
        $plate = $request->input('targa');
        $model = $request->input('modello');
        $personid = $request->input('persona');

        $newCar->plate = $plate;
        $newCar->model = $model;




        $newCar->save();

        if($personid != "err"){
            $person = Person::find($personid);
            $newCar->owner()->attach($person);
        }


        return redirect('/show');

    }

    public function register(){

        $people = Person::all();

        return view('new-car')->with('data',$people);

    }

}
