<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{

    public function car()
    {

        return $this->belongsToMany(Car::class);

    }

}
