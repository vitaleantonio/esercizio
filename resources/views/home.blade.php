<html>

    <head>

        <title>Gestione auto</title>

    </head>

    <style>

        .nav{

            height: 35px;
            border-bottom: solid 1px #969696;
            -webkit-box-shadow: 0px 0px 5px dimgrey;
            -moz-box-shadow: 0px 0px 5px dimgrey;
            box-shadow: 0px 0px 5px dimgrey;
            display: flex;
            flex-direction: row;
            padding: 10px;
            padding-top: 20px;
            padding-bottom: 0;

        }

        .nav-el{

            height: 40px;
            position: relative;
            right: 0;
            margin: 0 10px;
            margin-right: 0;

        }

        .nav-last{

            margin: 0 auto !important;
            margin-right: 0px !important;
        }

        .logo{

            height: 40px;
            position: relative;
            right: 0;
            margin: 0 10px;
            margin-right: 0;
            text-align: center;

        }

        a{

            text-decoration: none;
            color: gray;

        }

        a:hover{

            color: black;

        }

    </style>

    <body style="margin: 0px;">

        <div class="nav">

            <div class="logo">

                <a href="/">Gestione auto LARAVEL</a>

            </div>

            <div class="nav-el nav-last">

                <a href="/owner">Inserisci proprietario</a>

            </div>

            <div class="nav-el">

                <a href="/car">Inserisci auto</a>

            </div>

            <div class="nav-el">

                <a href="/show">Visualizza informazioni</a>

            </div>

        </div>

    </body>

</html>