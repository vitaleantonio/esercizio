
<form action="nw_ow" method="post">
    {{ csrf_field() }}
    <input type="text" placeholder="Inserisci il nome" name="nome">
    <input type="text" placeholder="Inserisci il cognome" name="cognome">
    <br />
    <br />
    <label>Seleziona una macchina già registrata</label>
    <select name="macchina">
        @if(count($data)>0)

            @foreach($data as $info)

                <option value="{{$info->id}}">{{ $info->plate }} {{ $info->model }}</option>

            @endforeach

        @else
            <option value="err">NO DATA</option>
        @endif

    </select>

    <button type="submit">Inserisci</button>
</form>

<a href="/">Home</a>