<html>

    <head>

        <title>Gestione auto</title>

    </head>

    <style>

        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }

    </style>

    <body>

    @if(count($data)>0)

        <table>
            <tr>
                <th>Modello</th>
                <th>Targa</th>
                <th>Proprietario</th>
            </tr>
        @foreach($data as $info)

                <tr>
                    <td>{{ $info->model }}</td>
                    <td>{{ $info->plate }}</td>
                    <td>
                        @foreach($info->owner as $owner)
                        {{ $owner->name }} {{ $owner->surname }} <br />
                        @endforeach
                    </td>
                </tr>

        @endforeach
        </table>

    @else
        <p> Nessun dato trovato, <a href="/">torna indietro</a> </p>
    @endif

    <a href="/">Home</a>

    </body>

</html>