
<form action="nw_cr" method="post">
    {{ csrf_field() }}
    <input type="text" placeholder="Inserisci targa" name="targa">
    <input type="text" placeholder="Inserisci modello" name="modello">
    <br />
    <br />
    <label>Seleziona il proprietario</label>
    <select name="persona">
        @if(count($data)>0)

            @foreach($data as $info)

                <option value="{{$info->id}}">{{ $info->name }} {{ $info->surname }}</option>

            @endforeach

        @else
            <option value="err">NO DATA</option>
        @endif

    </select>

    <button type="submit">Inserisci</button>

</form>

<a href="/">Home</a>